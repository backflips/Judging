<div class="col-xs-10 col-xs-offset-1">
    <form class="form-inline record-form" method="post" action="tracking.php">
        <div class="form-group top-space">
            <label for="startDateInput">Start</label>
            <input type="date" class="form-control" id="startDateInput" name="start" value="<?php echo (isset($_POST['start']) && validateDate($_POST['start'])) ? $_POST['start']: date('Y-m-d',strtotime("-1 day -1 year")); ?>">
        </div>
        <div class="form-group top-space">
            <label for="endDateInput">End</label>
            <input type="date" class="form-control" id="endDateInput" name="end" value="<?php echo (isset($_POST['end']) && validateDate($_POST['end'])) ? $_POST['end'] : date('Y-m-d')?>">
        </div>
        <button type="submit" class="btn btn-primary top-space">Get Records</button>
        <a href="tracking.php?add_record" class="btn btn-success top-space"><span class="glyphicon glyphicon-plus"></span> Add Record</a>
        <a href="tracking.php?print" class="btn btn-info btn-right top-space"><span class="glyphicon glyphicon-print"></span> Print</a>
        <button type="submit" class="btn btn-info btn-right top-space" name="save"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
    </form>
<?php
if($records = $record->getUserRecords($_SESSION['user_id'], (isset($_POST['start']) && validateDate($_POST['start'])) ? $_POST['start']: date('Y-m-d',strtotime("-1 day -1 year")) , (isset($_POST['start']) && validateDate($_POST['end'])) ? $_POST['end']: date('Y-m-d'))) {
?>
    <div class="table-responsive top-space">
    <table class="table table-bordered">
        <thead>
                <tr>
                    <th>Date</th>
                    <th>Competition</th>
                    <th>Location</th>
                    <th class="col-xs-1">Levels</th>
                    <th></th>
                    <th>Head Judge</th>
                </tr>
            </thead>
            <tbody> <?php foreach($records as $r) { ?>  
                <tr>
                    <td><strong><?php echo $r->record_date;?></strong>
                    <form method="post">
                        <button type="submit" class="btn btn-primary top-space2" name="edit_record" value="<?php echo $r->record_id;?>"><span class="glyphicon glyphicon-pencil"></span> Edit</button><br />
                        <button type="submit" class="btn btn-danger top-space" name="del_record" value="<?php echo $r->record_id;?>"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </form>
                    </td>
                    <td><?php echo $r->competition_name;?></td>
                    <td><?php echo $r->competition_city;?></td>
                    <td><?php echo $r->competition_levels;?></td>
                    <td class="col-xs-1">
                        <table class="table table-striped table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th> </th>
                                    <th>Floor</th>
                                    <th>Pommel</th>
                                    <th>Rings</th>
                                    <th>Vault</th>
                                    <th>PBars</th>
                                    <th>HBar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>c</td>
                                    <td><?php if($r->floor_compulsory) echo "x";?></td>
                                    <td><?php if($r->pommel_compulsory) echo "x";?></td>
                                    <td><?php if($r->rings_compulsory) echo "x";?></td>
                                    <td><?php if($r->vault_compulsory) echo "x";?></td>
                                    <td><?php if($r->pbars_compulsory) echo "x";?></td>
                                    <td><?php if($r->hbar_compulsory) echo "x";?></td>
                                </tr>
                                <tr>
                                    <td>o</td>
                                    <td><?php if($r->floor_optional) echo "x";?></td>
                                    <td><?php if($r->pommel_optional) echo "x";?></td>
                                    <td><?php if($r->rings_optional) echo "x";?></td>
                                    <td><?php if($r->vault_optional) echo "x";?></td>
                                    <td><?php if($r->pbars_optional) echo "x";?></td>
                                    <td><?php if($r->hbar_optional) echo "x";?></td>
                                </tr>
                                <tr>
                                    <td>f</td>
                                    <td><?php if($r->floor_finals) echo "x";?></td>
                                    <td><?php if($r->pommel_finals) echo "x";?></td>
                                    <td><?php if($r->rings_finals) echo "x";?></td>
                                    <td><?php if($r->vault_finals) echo "x";?></td>
                                    <td><?php if($r->pbars_finals) echo "x";?></td>
                                    <td><?php if($r->hbar_finals) echo "x";?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td><?php echo $r->head_judge;?></td>
                </tr><?php }?>
            </tbody>
        </table>
    </div>

 <?php
}
?>
</div>
