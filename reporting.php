<?php if(isset($login)) {
    if($login->errors) {
        foreach ($login->errors as $error) {
?>
<div class="container">
    <div class="alert alert-danger text-center" role="alert">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <?php echo $error; ?>
    </div>
</div>
<?php
        }
    }
    if($login->messages) {
        foreach ($login->messages as $message) {
?>
<div class="container">
    <div class="alert alert-success text-center" role="alert">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <?php echo $message; ?>
    </div>
</div>
<?php
        }
    }
}

?>