<?php
require_once('Login.php');

$login = new Login();

if($login->isUserLoggedIn()){
    header('Location: index.php');
}

require_once('_header.php');
require_once('message_helper.php');
?>
<?php if(!$login->isRegistered()) {
?>
<div class="container nav-spacer col-sm-8 col-sm-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading"><strong>Judges Registration</strong></div>
        <div class="panel-body">
            <div class="row">
            <form method="post" action="register.php" name="loginform" class="form-horizontal">
                <div class="form-group">
                    <label for="usernameInput" class="col-xs-2 col-xs-offset-2 control-label">Username</label>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" id="usernameInput" name="user_name" placeholder="Username">
                    </div>
                </div>
                <div class="form-group">
                    <label for="userEmailInput" class="col-xs-2 col-xs-offset-2 control-label">Email</label>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" id="userEmailInput" name="user_email" placeholder="Email Address">
                    </div>
                </div>
                <div class="form-group">
                    <label for="passwordInput" class="col-xs-2 col-xs-offset-2 control-label">Password</label>
                    <div class="col-xs-6">
                        <input type="password" class="form-control" id="passwordInput" name="user_password" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="passwordRetypeInput" class="col-xs-2 col-xs-offset-2 control-label">Password</label>
                    <div class="col-xs-6">
                        <input type="password" class="form-control" id="passwordRetypeInput" name="user_password_retype" placeholder="Password">
                    </div>
                </div>
                <div class="col-xs-6 col-xs-offset-4">
                <button type="submit" class="btn btn-success btn-block" name="register">Register</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div><?php }
else { ?>
<div class="container nav-spacer col-sm-8 col-sm-offset-2">
    <h2 class="text-center">You were successfully registered.</h2>
    <p class="text-center">The administrator has been notified.  If they verify your account then you will be allowed to log in.</p>
    <div class="text-center">
        <a class="btn btn-primary" href="login.php">Go to Login</a>
    </div>
</div>

<?php
    unset($_SESSION['user_is_registered']);
} ?>

<?php require_once('_footer.php');?>
