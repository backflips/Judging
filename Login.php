<?php
    class Login
    {
        protected $db_connection = null;
        
        private $user_id = null;
        
        private $user_name = "";
        
        public $errors = array();
        
        public $messages = array();
        
        private $passwordChanged = false;
        
        public function __construct() {
            
            require_once('config.php');
            require_once('includes/messages.php');
            
            session_start();
            
            
            if(isset($_POST['register'])) {
                $this->registerNewUser($_POST['user_name'], $_POST['user_email'], $_POST['user_password'], $_POST['user_password_retype']);
            }
            
            if(isset($_GET["logout"])) {
                $this->Logout();
            }
            else if(isset($_POST["login"])) {
                $this->loginPost($_POST['user_name'], $_POST['user_password']);
            }
            
            
            if(!empty($_SESSION['user_name']) && ($_SESSION['user_logged_in'] == 1)) {
                $this->loginSession();
                if(isset($_POST['save_password'])) {
                    $this->changePassword($_POST['oldPassword'], $_POST['newPassword'], $_POST['confirmPassword']);
                }
                else if(isset($_POST['save_account'])) {
                    $this->editAccount($_POST['username'], $_POST['email']);
                }
            }
        }
        
        protected function databaseConnect() {
            if ($this->db_connection != null) {
                return true;
            }
            else {
                try {
                    $this->db_connection = new PDO('mysql:host='. DB_HOST . ';dbname='. DB_NAME . ';charset=utf8', DB_USER, DB_PASS);
                    return true;
                } catch(PDOException $e) {
                    $this->errors[] = DATABASE_ERROR . $e->getMessage();
                }
            }
            return false;
        }
        
        private function increaseFailedLoginCount($userid) {
            if(!isset($this->db_connection)){
                $this->databaseConnect();
            }
            $sth = $this->db_connection->prepare('UPDATE user_login SET user_failed_logins = user_failed_logins+1, user_last_failed_login = :user_last_failed_login WHERE user_id = :user_id');
            $sth->execute(array(':user_id' => $userid, ':user_last_failed_login' => time()));
            
        }
        
        private function resetFailedLoginCount($userid) {
            if(!isset($this->db_connection)){
                $this->databaseConnect();
            }
            $sth = $this->db_connection->prepare('UPDATE user_login SET user_failed_logins = 0, user_last_failed_login = NULL WHERE user_id = :user_id AND user_failed_logins != 0');
            $sth->execute(array(':user_id' => $userid));
        }
        
        private function getUserData($user_name){
            if($this->databaseConnect()) {
                $query = $this->db_connection->prepare('SELECT * FROM user_login WHERE user_name= :user_name');
                $query->bindValue(':user_name', $user_name, PDO::PARAM_STR);
                $query->execute();
                return $query->fetchObject();
            }
            else {
                return false;
            }
        }
        
        private function getUserDataEmail($user_email) {
            if ($this->databaseConnect()) {
                $query = $this->db_connection->prepare('SELECT * FROM user_login WHERE user_email= :user_email');
                $query->bindValue(':user_email', $user_email, PDO::PARAM_STR);
                $query->execute();
                return $query->fetchObject();
            }
            else {
                return false;
            }
        }
        
        private function loginPost($user_name, $user_password) {
            if(empty($user_name)) {
                $this->errors[] = USERNAME_EMPTY;
            }
            else if(empty($user_password)) {
                $this->errors[] = PASSWORD_EMPTY;
            }
            else {
                if(!filter_var($user_name, FILTER_VALIDATE_EMAIL)) {
                    $user_name = filter_var($user_name, FILTER_SANITIZE_STRING);
                    $user_data_row = $this->getUserData(trim($user_name));
                }
                else {
                    $user_email = filter_var($user_name, FILTER_SANITIZE_EMAIL);
                    if($user_name != $user_email) {
                        $this->error[] = INVALID_EMAIL;
                    }
                    else {
                        $user_data_row = $this->getUserDataEmail(trim($user_email));
                    }
                }
                
                if(!isset($user_data_row->user_id)){
                    $this->errors[] = LOGIN_FAILED;
                }
                else if(($user_data_row->user_failed_logins >= 3) && ($user_data_row->user_last_failed_login > (time() - 30))) {
                    $this->errors[] = LOGIN_FAILED_TOOMANY;
                }
                else if(!password_verify($user_password, $user_data_row->user_password_hash)) {
                    $this->increaseFailedLoginCount($user_data_row->user_id);
                    $this->errors[] = LOGIN_FAILED;
                }
                else if ($user_data_row->user_verified != 1) {
                    $this->errors[] = ACCOUNT_NOT_VERIFIED;
                }
                else {
                    $_SESSION['user_id'] = $user_data_row->user_id;
                    $_SESSION['user_name'] = $user_data_row->user_name;
                    $_SESSION['user_email'] = $user_data_row->user_email;
                    $_SESSION['user_logged_in'] = 1;
                    
                    $this->setSessionTimer();
                    $this->resetFailedLoginCount($user_data_row->user_id);
                    
                    $this->messages[] = LOGGED_IN;
                }
            }
        }
        
        private function registerNewUser($username, $email, $password, $passwordRetype) {
            if(empty($username)){
                $this->errors[] = USERNAME_EMPTY;
            }
            else if(empty($password)) {
                $this->errors[] = PASSWORD_EMPTY;
            }
            else if(empty($passwordRetype)) {
                $this->errors[] = PASSWORD_RETYPE_EMPTY;
            }
            else if($password !== $passwordRetype) {
                $this->errors[] = PASSWORDS_DONT_MATCH;
            }
            else if((strlen($username) < 3) || strlen($username) > 64 || (!preg_match('/^[a-zA-Z][a-zA-Z0-9]{3,64}$/',$username))) {
                $this->errors[] = USERNAME_INVALID;
            }
            else if((strlen($password) < 6)) {
                $this->errors[] = PASSWORD_INVALID;
            }
            else if(empty($email)) {
                $this->errors[] = EMAIL_EMPTY;
            }
            else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->errors[] = EMAIL_INVALID;
            }
            else if($email != filter_var($email, FILTER_SANITIZE_EMAIL)) {
                $this->errors[] = EMAIL_INVALID;
            }
            else if(strlen($email) > 254) {
                $this->errors[] = EMAIL_INVALID;
            }
            else{
                $user_data = $this->getUserData($username);
                
                if(isset($user_data->user_id)) {
                    $this->errors[] = USERNAME_ALREADY_EXISTS;
                    return;
                }
                else {
                    $user_data = $this->getUserDataEmail($email);
                }
                
                if(isset($user_data->user_id)){
                    $this->errors[] = EMAIL_ALREADY_EXISTS;
                    return;
                }      
                else {
                    $password_hash = $this->getPasswordHash($password);
                    
                    $insert_query = $this->db_connection->prepare('INSERT INTO user_login (user_name, user_password_hash, user_email, user_registration_ip, user_registration_datetime) VALUES(:user_name, :user_password_hash, :user_email, :user_registration_ip, now())');
                    $insert_query->bindValue(':user_name', $username, PDO::PARAM_STR);
                    $insert_query->bindValue(':user_password_hash', $password_hash, PDO::PARAM_STR);
                    $insert_query->bindValue(':user_email', $email, PDO::PARAM_STR);
                    $insert_query->bindValue(':user_registration_ip', $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
                    $insert_query->execute();
                    
                    $_SESSION['user_is_registered'] = true;
                }
            }
        }
        
        private function loginSession() {
            
            if($this->checkSessionTimeout()) {
                $this->setSessionTimer();
            }
            else {
                $this->Logout(true);
                
                $this->errors[] = SESSION_TIMEOUT_MESSAGE;
            }            
        }
        
        private function setSessionTimer() {
            $_SESSION['start_time'] = time();
        }
        
        private function checkSessionTimeout() {
            if(isset($_SESSION['start_time'])) {
                $time = $_SESSION['start_time'];
                
                if((time() - $time) >= SESSION_TIMEOUT) {
                        return false;
                }
                else return true;
            }
            else {
                return false;
            }
        }
        
        public function Logout($silent = false) {
            $_SESSION = array();
            session_destroy();
            $this->user_logged_in = false;
            if(!$silent) {
                $this->messages[] = LOGGED_OUT;      
            }          
        }
        
        public function isUserLoggedIn() {
            return (!empty($_SESSION['user_name']) && $_SESSION['user_logged_in'] == 1) ? true : false;
        }
        
        public function isRegistered() {
            $this->messages[] = REGISTRATION_SUCCESSFULL;
            return (isset($_SESSION['user_is_registered']) && $_SESSION['user_is_registered'] == 1) ? true : false;            
        }
        
        private function getPasswordHash($password) {
            $hash_cost_factor = (defined('HASH_COST_FACTOR') ? HASH_COST_FACTOR : null);
            
            return password_hash($password, PASSWORD_DEFAULT, array('cost' => $hash_cost_factor));
        }
        
        public function hasPasswordChanged() {
            return $this->passwordChanged;
        }
        
        public function hasAccountChangeFailed() {
            return $this->accountChangeFail;
        }
        
        protected function changePassword($old, $new, $confirm) {
            if(empty($old) || empty($new) || empty($confirm)) {
                $this->errors[] = MISSING_REQUIRED;
            }
            else if($new !== $confirm) {
                $this->errors[] = PASSWORDS_DONT_MATCH;
            }
            else if(strlen($new) < 6) {
                $this->errors[] = PASSWORD_INVALID;
            }
            else {
                $user_data = $this->getUserData($_SESSION['user_name']);
                if(!isset($user_data->user_id)) {
                    $this->errors[] = DATABASE_ERROR;
                }
                else if(!password_verify($old, $user_data->user_password_hash)) {
                    $this->errors[] = PASSWORD_WRONG;
                }
                else if(password_verify($new, $user_data->user_password_hash)) {
                    $this->errors[] = PASSWORD_SAME_AS_OLD;                    
                }
                else if($this->databaseConnect()) {
                    $password_hash = $this->getPasswordHash($new);
                    
                    $query = $this->db_connection->prepare('UPDATE user_login SET user_password_hash = :user_password_hash
                                                           WHERE user_id = :user_id');
                    $query->bindValue(':user_password_hash', $password_hash, PDO::PARAM_STR);
                    $query->bindValue(':user_id', $user_data->user_id, PDO::PARAM_STR);
                    $query->execute();
                    
                    if($query->rowCount() == 1) {
                        $this->passwordChanged = true;
                        $this->messages[] = PASSWORD_CHANGED;
                    }
                    else {
                        $this->errors[] = PASSWORD_CHANGE_FAILED; 
                    } 
                }                
                else {
                    $this->errors[] = DATABASE_ERROR;
                }
            }
        }
        
        protected function editAccount($username, $email) {
            $_POST['edit_account'] = true;
            $oldUsername = $_SESSION['user_name'];
            $oldEmail = $this->getUserData($oldUsername)->user_email;
            
            
            if(empty($username)) {
                $this->errors[] = USERNAME_EMPTY;
            }
            else if(empty($email)) {
                $this->errors[] = EMAIL_EMPTY;
            }
            else if(!preg_match('/^[a-zA-Z0-9]{3,64}$/', $username)){
                $this->errors[] = USERNAME_INVALID;
            }
            else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->errors[] = EMAIL_INVALID;
            }
            else if($email != filter_var($email, FILTER_SANITIZE_EMAIL)) {
                $this->errors[] = EMAIL_INVALID;
            }
            else if(isset($this->getUserData($username)->user_id) && $username != $oldUsername) {
                $this->errors[] = USERNAME_ALREADY_EXISTS;
            }
            else if(isset($this->getUserDataEmail($email)->user_id) && $email != $oldEmail) {
                $this->errors[] = EMAIL_ALREADY_EXISTS;
            }
            else if($username == $oldUsername && $email == $oldEmail) {
                $this->errors[] = ACCOUNT_INFO_SAME;
            }
            else {
                $query = $this->db_connection->prepare('UPDATE user_login SET user_name = :username, user_email = :email WHERE user_id = :user_id');
                $query->bindValue(':username', $username, PDO::PARAM_STR);
                $query->bindValue(':email', $email, PDO::PARAM_STR);
                $query->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_STR);
                $query->execute();
                
                if($query->rowCount() == 1) {
                    $this->messages[] = ACCOUNT_INFO_CHANGED;
                    $this->user_name = $username;
                    $_SESSION['user_name'] = $username;
                    $_SESSION['user_email'] = $email;
                    unset($_POST['edit_account']);                    
                }
                else {
                    $this->errors[] = ACCOUNT_INFO_CHANGE_FAILED;
                }
            }
            
        }
    }

?>