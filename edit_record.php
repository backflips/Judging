<?php $r = $record->getRecord($_POST['edit_record']); ?>
<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
    <h2>Edit record</h2>
    <form method="post" action="tracking.php" name="recordForm">
        <div class="form-group">
            <label class="control-label">Competition Date</label>
            <input type="date" class="form-control" id="inputDate" name="date" value="<?php echo $r->record_date;?>">
        </div>
        <div class="form-group">
            <label class="control-label">Competition Name</label>
            <input type="text" class="form-control" id="inputName" name="name" value="<?php echo $r->competition_name?>">
        </div>
        <div class="form-group">
            <label class="control-label">Competition Location</label>
            <input type="text" class="form-control" id="inputLocation" name="location" value="<?php echo $r->competition_city?>">
        </div>
        <div class="form-group">
            <label class="control-label">Levels</label>
            <textarea class="form-control" id="inputLevels" name="levels" rows="2"><?php echo $r->competition_levels?></textarea>
        </div>
        <div class="col-xs-6 col-md-2 form-group">
            <label>Floor</label>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="floorComp" <?php echo ($r->floor_compulsory) ? "checked" : "";?>>
                    Compulsory
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="floorOpt" <?php echo ($r->floor_optional) ? "checked" : "";?>>
                    Optional
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="floorFin" <?php echo ($r->floor_finals) ? "checked" : "";?>>
                    Finals
                </label>
            </div>
        </div>
        <div class="col-xs-6 col-md-2 form-group">
            <label>Pommel Horse</label>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pommelComp" <?php echo ($r->pommel_compulsory) ? "checked" : "";?>>
                    Compulsory
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pommelOpt" <?php echo ($r->pommel_optional) ? "checked" : "";?>>
                    Optional
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pommelFin" <?php echo ($r->pommel_finals) ? "checked" : "";?>>
                    Finals
                </label>
            </div>
        </div>
        <div class="col-xs-6 col-md-2 form-group">
            <label>Rings</label>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="ringsComp" <?php echo ($r->rings_compulsory) ? "checked" : "";?>>
                    Compulsory
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="ringsOpt" <?php echo ($r->rings_optional) ? "checked" : "";?>>
                    Optional
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="ringsFin" <?php echo ($r->rings_finals) ? "checked" : "";?>>
                    Finals
                </label>
            </div>
        </div>
        <div class="col-xs-6 col-md-2 form-group">
            <label>Vault</label>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="vaultComp" <?php echo ($r->vault_compulsory) ? "checked" : "";?>>
                    Compulsory
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="vaultOpt" <?php echo ($r->vault_optional) ? "checked" : "";?>>
                    Optional
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="vaultFin" <?php echo ($r->vault_finals) ? "checked" : "";?>>
                    Finals
                </label>
            </div>
        </div>
        <div class="col-xs-6 col-md-2 form-group">
            <label>Parallel Bars</label>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pbarsComp" <?php echo ($r->pbars_compulsory) ? "checked" : "";?>>
                    Compulsory
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pbarsOpt" <?php echo ($r->pbars_optional) ? "checked" : "";?>>
                    Optional
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pbarsFin" <?php echo ($r->pbars_finals) ? "checked" : "";?>>
                    Finals
                </label>
            </div>
        </div>
        <div class="col-xs-6 col-md-2 form-group">
            <label>Horizontal Bar</label>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="hbarComp" <?php echo ($r->hbar_compulsory) ? "checked" : "";?>>
                    Compulsory
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="hbarOpt" <?php echo ($r->hbar_optional) ? "checked" : "";?>>
                    Optional
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="hbarFin" <?php echo ($r->hbar_finals) ? "checked" : "";?>>
                    Finals
                </label>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">Head Judge</label>
            <input type="text" class="form-control" id="inputHeadJudge" name="headJudge" value="<?php echo $r->head_judge?>">
        </div>
        <div class="col-xs-6 col-sm-3">
            <button type="submit" class="btn btn-success btn-block" name="save_edit_record" value="<?php echo $r->record_id?>">Save</button>
        </div>
        <div class="col-xs-6 col-sm-3">
            <a class="btn btn-warning btn-block" href="tracking.php">Cancel</a>
        </div>
    </form>
</div>