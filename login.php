<?php
require_once('Login.php');

$login = new Login();

if($login->isUserLoggedIn()){
    header('Location: index.php');
}


require_once('_header.php');
require_once('message_helper.php');
?>
<div class="container nav-spacer col-sm-8 col-sm-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading"><strong>Judges Login</strong></div>
        <div class="panel-body">
            <div class="row">
            <form method="post" action="login.php" name="loginform" class="form-horizontal">
                <div class="form-group">
                    <label for="usernameInput" class="col-xs-2 col-xs-offset-2 control-label">Username</label>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" id="usernameInput" name="user_name" placeholder="Username or Email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="passwordInput" class="col-xs-2 col-xs-offset-2 control-label">Password</label>
                    <div class="col-xs-6">
                        <input type="password" class="form-control" id="passwordInput" name="user_password" placeholder="Password">
                    </div>
                </div>
                <div class="col-xs-6 col-xs-offset-4">
                <button type="submit" class="btn btn-success btn-block" name="login"><span class="glyphicon glyphicon-log-in"></span> Login</button>
                </div>
            </form>
            </div>
            <div class="row">
                <div class="col-xs-6 col-xs-offset-4">
                <p id="register-text" class="text-center"><strong>Or <a href="register.php">Register</a></strong></p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once('_footer.php');?>
