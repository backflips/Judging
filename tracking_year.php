<form method="post" action="tracking.php" name="recordform" class="form-horizontal">
    <div class="form-group">
        <label class="control-label col-sm-2">Select Season</label>
        <div class="col-sm-8">
            <select class="form-control" name="season">
                
            </select>
        </div>
    </div>
    <div class="form-group">
        <input type="date" name="date">
    </div>
    <div class="col-xs-6 col-sm-2 col-sm-offset-2">
        <button type="submit" class="btn btn-success btn-block" name="set_date">Save</button>
    </div>
    <div class="col-xs-6 col-sm-2">
        <a class="btn btn-warning btn-block" href="settings.php?account">Cancel</a>
    </div>
</form>