
<div class="container">
    <div class="page-header">
        <h1>Certifications</h1>
    </div>
    <?php if($login->getUserCerts()) {
        foreach($login->getUserCerts() as $cert) {?>
    <div class="col-sm-3">
        <div class="panel panel-default">
            <div class="panel-heading"><strong><?php echo $cert->discipline_name; ?></strong></div>
            <div class="panel-body">
                <label>Category</label><p><?php echo $cert->category_name; ?></p>
            </div>
        </div>
    </div>
    <?php }
    } else {
        ?>
        <?php }?>
</div>
