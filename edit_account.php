<div class="panel panel-default">
    <div class="panel-heading"><strong>Account Information</strong></div>
    <div class="panel-body">
        <form method="post" action="settings.php?account" name="accountform" class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2">Username</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputUsername" name="username" value="<?php echo $_SESSION['user_name']; ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Email</label>
                <div class="col-sm-8">
                    <input type="email" class="form-control" id="inputEmail" name="email" value="<?php echo $_SESSION['user_email']; ?>">
                </div>
            </div>
            <div class="col-xs-6 col-sm-5 col-sm-offset-1 col-md-3 col-md-offset-2">
                <button type="submit" class="btn btn-success btn-block" name="save_account">Save</button>
            </div>
            <div class="col-xs-6 col-sm-5 col-md-3">
                <a class="btn btn-warning btn-block" href="settings.php?account">Cancel</a>
            </div>
        </form>
    </div>
</div>
