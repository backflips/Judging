<?php $r = $record->getRecord($_POST['del_record']); ?>
<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading"><strong>Confirm Delete</strong></div>
        <div class="panel-body">
            <h4>Are you sure you want to delete this record?</h4>
            <form method="post" action="tracking.php" name="accountform" class="form-horizontal">
                <div class="table-responsive top-space">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Competition</th>
                                <th>Location</th>
                                <th class="col-xs-1">Levels</th>
                                <th></th>
                                <th>Head Judge</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><strong><?php echo $r->record_date;?></strong>
                                </td>
                                <td><?php echo $r->competition_name;?></td>
                                <td><?php echo $r->competition_city;?></td>
                                <td><?php echo $r->competition_levels;?></td>
                                <td class="col-xs-1">
                                    <table class="table table-striped table-condensed table-bordered">
                                        <thead>
                                            <tr>
                                                <th> </th>
                                                <th>Floor</th>
                                                <th>Pommel</th>
                                                <th>Rings</th>
                                                <th>Vault</th>
                                                <th>PBars</th>
                                                <th>HBar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>c</td>
                                                <td><?php if($r->floor_compulsory) echo "x";?></td>
                                                <td><?php if($r->pommel_compulsory) echo "x";?></td>
                                                <td><?php if($r->rings_compulsory) echo "x";?></td>
                                                <td><?php if($r->vault_compulsory) echo "x";?></td>
                                                <td><?php if($r->pbars_compulsory) echo "x";?></td>
                                                <td><?php if($r->hbar_compulsory) echo "x";?></td>
                                            </tr>
                                            <tr>
                                                <td>o</td>
                                                <td><?php if($r->floor_optional) echo "x";?></td>
                                                <td><?php if($r->pommel_optional) echo "x";?></td>
                                                <td><?php if($r->rings_optional) echo "x";?></td>
                                                <td><?php if($r->vault_optional) echo "x";?></td>
                                                <td><?php if($r->pbars_optional) echo "x";?></td>
                                                <td><?php if($r->hbar_optional) echo "x";?></td>
                                            </tr>
                                            <tr>
                                                <td>f</td>
                                                <td><?php if($r->floor_finals) echo "x";?></td>
                                                <td><?php if($r->pommel_finals) echo "x";?></td>
                                                <td><?php if($r->rings_finals) echo "x";?></td>
                                                <td><?php if($r->vault_finals) echo "x";?></td>
                                                <td><?php if($r->pbars_finals) echo "x";?></td>
                                                <td><?php if($r->hbar_finals) echo "x";?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td><?php echo $r->head_judge;?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-6 col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-3">
                    <button type="submit" class="btn btn-danger btn-block" name="delete_record" value="<?php echo $r->record_id;?>">Yes, delete it.</button>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <a class="btn btn-warning btn-block" href="tracking.php">No, cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>