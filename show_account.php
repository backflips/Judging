
<div class="panel panel-default">
    <div class="panel-heading"><strong>Account Information</strong></div>
    <div class="panel-body">
        <form method="post" action="settings.php?account" name="profileform" class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-xs-2">Username</label>
                <p class="form-control-static col-xs-10"><?php echo $_SESSION['user_name']; ?></p>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-2">Email</label>
                <p class="form-control-static col-xs-10"><?php echo $_SESSION['user_email']; ?></p>
            </div>
            <div class="col-xs-6 col-sm-5 col-sm-offset-1 col-md-3 col-md-offset-2">
                <button type="submit" class="btn btn-primary btn-block" name="edit_account">Edit</button>
            </div>
            <div class="col-xs-6 col-sm-5 col-md-3">
                <a class="btn btn-info btn-block" href="settings.php?password">Change Password</a>
            </div>
        </form>
    </div>
</div>
