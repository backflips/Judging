<?php
    class Login
    {
        private $db_connection = null;
        
        private $user_id = null;
        
        private $user_name = "";
        
        private $user_email = "";
        
        public $errors = array();
        
        public $messages = array();
        
        public function __construct() {
            
            require_once('config.php');
            require_once('includes/messages.php');
            
            session_start();
            
            if(isset($_GET["logout"])) {
                $this->Logout();
            }
            else if(isset($_POST["login"])) {
                $this->loginPost($_POST['user_name'], $_POST['user_password']);
            }
            else if(!empty($_SESSION['user_name']) && ($_SESSION['user_logged_in'] == 1)) {
                $this->loginSession();
            }
        }
        
        private function databaseConnect() {
            if ($this->db_connection != null) {
                return true;
            }
            else {
                try {
                    $this->db_connection = new PDO('mysql:host='. DB_HOST . ';dbname='. DB_NAME . ';charset=utf8', DB_USER, DB_PASS);
                    return true;
                } catch(PDOException $e) {
                    $this->errors[] = DATABASE_ERROR . $e->getMessage();
                }
            }
            return false;
        }
        
        private function increaseFailedLoginCount($userid) {
            if(!isset($this->db_connection)){
                $this->databaseConnect();
            }
            $sth = $this->db_connection->prepare('UPDATE users SET user_failed_logins = user_failed_logins+1, user_last_failed_login = :user_last_failed_login WHERE user_id = :user_id');
            $sth->execute(array(':user_id' => $userid, ':user_last_failed_login' => time()));
            
        }
        
        private function resetFailedLoginCount($userid) {
            if(!isset($this->db_connection)){
                $this->databaseConnect();
            }
            $sth = $this->db_connection->prepare('UPDATE users SET user_failed_logins = 0, user_last_failed_login = NULL WHERE user_id = :user_id AND user_failed_logins != 0');
            $sth->execute(array(':user_id' => $userid));
        }
        
        private function getUserData($user_name){
            if($this->databaseConnect()) {
                $query = $this->db_connection->prepare('SELECT * FROM users WHERE user_name= :user_name');
                $query->bindValue(':user_name', $user_name, PDO::PARAM_STR);
                $query->execute();
                return $query->fetchObject();
            }
            else {
                return false;
            }
        }
        
        private function getUserDataEmail($user_email) {
            if ($this->databaseConnect()) {
                $query = $this->db_connection->prepare('SELECT * FROM users WHERE user_email= :user_email');
                $query->bindValue(':user_email', $user_email, PDO::PARAM_STR);
                $query->execute();
                return $query->fetchObject();
            }
            else {
                return false;
            }
        }
        
        private function loginPost($user_name, $user_password) {
            if(empty($user_name)) {
                $this->errors[] = USERNAME_EMPTY;
            }
            else if(empty($user_password)) {
                $this->errors[] = PASSWORD_EMPTY;
            }
            else {
                if(!filter_var($user_name, FILTER_VALIDATE_EMAIL)) {
                    $user_name = filter_var($user_name, FILTER_SANITIZE_STRING);
                    $user_data_row = $this->getUserData(trim($user_name));
                }
                else {
                    $user_email = filter_var($user_name, FILTER_SANITIZE_EMAIL);
                    if($user_name != $user_email) {
                        $this->error[] = INVALID_EMAIL;
                    }
                    else {
                        $user_data_row = $this->getUserDataEmail(trim($user_email));
                    }
                }
                
                if(!isset($user_data_row->user_id)){
                    $this->errors[] = LOGIN_FAILED;
                }
                else if(($user_data_row->user_failed_logins >= 3) && ($user_data_row->user_last_failed_login > (time() - 30))) {
                    $this->errors[] = LOGIN_FAILED_TOOMANY;
                }
                else if(!password_verify($user_password, $user_data_row->user_password_hash)) {
                    $this->increaseFailedLoginCount($user_data_row->user_id);
                    $this->errors[] = LOGIN_FAILED;
                }
                else if ($user_data_row->user_verified != 1) {
                    $this->errors[] = ACCOUNT_NOT_VERIFIED;
                }
                else {
                    $_SESSION['user_id'] = $user_data_row->user_id;
                    $_SESSION['user_name'] = $user_data_row->user_name;
                    $_SESSION['user_email'] = $user_data_row->user_email;
                    $_SESSION['user_logged_in'] = 1;
                    
                    $this->setSessionTimer();
                    $this->resetFailedLoginCount($user_data_row->user_id);
                    
                    $this->messages[] = LOGGED_IN;
                }
            }
        }
        
        private function loginSession() {
            
            if($this->checkSessionTimeout()) {
                $this->setSessionTimer();
            }
            else {
                $this->Logout(true);
                
                $this->errors[] = SESSION_TIMEOUT_MESSAGE;
            }            
        }
        
        private function setSessionTimer() {
            $_SESSION['start_time'] = time();
        }
        
        private function checkSessionTimeout() {
            if(isset($_SESSION['start_time'])) {
                $time = $_SESSION['start_time'];
                
                if((time() - $time) >= SESSION_TIMEOUT) {
                        return false;
                }
                else return true;
            }
            else {
                return false;
            }
        }
        
        public function Logout($silent = false) {
            $_SESSION = array();
            session_destroy();
            
            $this->user_logged_in = false;
            if(!$silent) {
                $this->messages[] = LOGGED_OUT;      
            }          
        }
        
        public function isUserLoggedIn() {
            return (!empty($_SESSION['user_name']) && $_SESSION['user_logged_in'] == 1) ? true : false;
        }
    }

?>