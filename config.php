<?php
function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

/*
 *Database Configuration
 *
 *DB_HOST: The database host
 *DB_NAME: The database to use
 *DB_USER: The name of the user to use to connect to the database
 *DB_PASS: The user password
 *
 */
define("DB_HOST", "127.0.0.1");
define("DB_NAME", "tracking");
define("DB_USER", "root");
define("DB_PASS", "CYC10ne124");

define("SESSION_TIMEOUT", 3600);


/*
 *Cookie Configuration
 *
 *COOKIE_VALID_TIME: how long will a cookie be valid
 *COOKIE_DOMAIN: where is the cookie valid
 *COOKIE_SECRET_KEY: Random value
 */
define("COOKIE_RUNTIME", 1209600);
define("COOKIE_DOMAIN", ".127.0.0.1");
define("COOKIE_SECRET_KEY", "1az@TM?S{-$7(sf3lJFe-5gz");



define("HASH_COST_FACTOR", "10");

?>