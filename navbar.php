<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Tracking</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

<?php
if(!$login->isUserLoggedIn()) {
?>
            <ul class="nav navbar-nav navbar-right">
                <p class="navbar-text"><a href="login.php" class="navbar-link"><span class="glyphicon glyphicon-log-in"></span> <strong>Login</strong></a> or <a href="register.php" class="navbar-link"><strong>Register</strong></a></p>
            </ul>
<?php
}
else {
?>
            <ul class="nav navbar-nav">
                <li><a href="tracking.php"><span class="glyphicon glyphicon-list-alt"></span> Judging Record</a></li>
                <li><a href="settings.php"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><p class="navbar-text">Logged in as <a class="navbar-link"><?php echo $_SESSION['user_name']?></a></p></li>
                <li><a class="text-warning" href="index.php?logout"><span class="glyphicon glyphicon-log-out"></span> <strong>Logout</strong></a></li>
            </ul>
<?php
}
?>
        </div>
    </div>
</nav>

