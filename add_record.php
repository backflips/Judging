<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
    <h2>Add record</h2>
    <form method="post" action="tracking.php" name="recordForm">
        <div class="form-group">
            <label class="control-label">Competition Date</label>
            <input type="date" class="form-control" id="inputDate" name="date" <?php echo (isset($_POST['date'])) ? 'value="' . filter_var($_POST['date'], FILTER_SANITIZE_STRING) . '"' : "";?>>
        </div>
        <div class="form-group">
            <label class="control-label">Competition Name</label>
            <input type="text" class="form-control" id="inputName" name="name" <?php echo (isset($_POST['name'])) ? 'value="' . filter_var($_POST['name'], FILTER_SANITIZE_STRING) . '"' : "";?>>
        </div>
        <div class="form-group">
            <label class="control-label">Competition Location</label>
            <input type="text" class="form-control" id="inputLocation" name="location" <?php echo (isset($_POST['location'])) ? 'value="' . filter_var($_POST['location'], FILTER_SANITIZE_STRING) . '"' : "";?>>
        </div>
        <div class="form-group">
            <label class="control-label">Levels</label>
            <textarea class="form-control" id="inputLevels" name="levels" rows="2"><?php echo (isset($_POST['levels'])) ? filter_var($_POST['levels'], FILTER_SANITIZE_STRING) : "";?></textarea>
        </div>
        <div class="col-xs-6 col-md-2 form-group">
            <label>Floor</label>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="floorComp" <?php echo (isset($_POST['floorComp'])) ? "checked" : "";?>>
                    Compulsory
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="floorOpt" <?php echo (isset($_POST['floorOpt'])) ? "checked" : "";?>>
                    Optional
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="floorFin" <?php echo (isset($_POST['floorFin'])) ? "checked" : "";?>>
                    Finals
                </label>
            </div>
        </div>
        <div class="col-xs-6 col-md-2 form-group">
            <label>Pommel Horse</label>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pommelComp" <?php echo (isset($_POST['pommelComp'])) ? "checked" : "";?>>
                    Compulsory
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pommelOpt" <?php echo (isset($_POST['pommelOpt'])) ? "checked" : "";?>>
                    Optional
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pommelFin" <?php echo (isset($_POST['pommelFin'])) ? "checked" : "";?>>
                    Finals
                </label>
            </div>
        </div>
        <div class="col-xs-6 col-md-2 form-group">
            <label>Rings</label>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="ringsComp" <?php echo (isset($_POST['ringsComp'])) ? "checked" : "";?>>
                    Compulsory
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="ringsOpt" <?php echo (isset($_POST['ringsOpt'])) ? "checked" : "";?>>
                    Optional
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="ringsFin" <?php echo (isset($_POST['ringsFin'])) ? "checked" : "";?>>
                    Finals
                </label>
            </div>
        </div>
        <div class="col-xs-6 col-md-2 form-group">
            <label>Vault</label>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="vaultComp" <?php echo (isset($_POST['vaultComp'])) ? "checked" : "";?>>
                    Compulsory
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="vaultOpt" <?php echo (isset($_POST['vaultOpt'])) ? "checked" : "";?>>
                    Optional
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="vaultFin" <?php echo (isset($_POST['vaultFin'])) ? "checked" : "";?>>
                    Finals
                </label>
            </div>
        </div>
        <div class="col-xs-6 col-md-2 form-group">
            <label>Parallel Bars</label>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pbarsComp" <?php echo (isset($_POST['pbarsComp'])) ? "checked" : "";?>>
                    Compulsory
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pbarsOpt" <?php echo (isset($_POST['pbarsOpt'])) ? "checked" : "";?>>
                    Optional
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pbarsFin" <?php echo (isset($_POST['pbarsFin'])) ? "checked" : "";?>>
                    Finals
                </label>
            </div>
        </div>
        <div class="col-xs-6 col-md-2 form-group">
            <label>Horizontal Bar</label>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="hbarComp" <?php echo (isset($_POST['hbarComp'])) ? "checked" : "";?>>
                    Compulsory
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="hbarOpt" <?php echo (isset($_POST['hbarOpt'])) ? "checked" : "";?>>
                    Optional
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="hbarFin" <?php echo (isset($_POST['hbarFin'])) ? "checked" : "";?>>
                    Finals
                </label>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">Head Judge</label>
            <input type="text" class="form-control" id="inputHeadJudge" name="headJudge" <?php echo (isset($_POST['headJudge'])) ? 'value="' . filter_var($_POST['headJudge'], FILTER_SANITIZE_STRING) . '"' : "";?>>
        </div>
        <div class="col-xs-6 col-sm-3">
            <button type="submit" class="btn btn-success btn-block" name="save_record">Save</button>
        </div>
        <div class="col-xs-6 col-sm-3">
            <a class="btn btn-warning btn-block" href="tracking.php">Cancel</a>
        </div>
    </form>
</div>