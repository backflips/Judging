<?php
if(isset($login)) {
    $object = $login;
}
if(isset($login) && !$login->messages && isset($record)) {
    $object = $record;
}


if(isset($object)) {
    if($object->errors) {
        foreach ($object->errors as $error) {
?>

    <div class="alert alert-danger text-center" role="alert">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <?php echo $error; ?>
    </div>

<?php
        }
    }
    if($object->messages) {
        foreach ($object->messages as $message) {
?>

    <div class="alert alert-success text-center" role="alert">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <?php echo $message; ?>
    </div>

<?php
        }
    }
}

?>