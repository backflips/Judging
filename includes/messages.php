<?php
define("DATABASE_ERROR", "Problem connecting to database.  ");
define("USERNAME_EMPTY", "The username field was empty.");
define("USERNAME_INVALID", "The username entered was invalid.  Usernames must be 3-64 characters long, start with a letter and only contain alphanumeric characters");
define("USERNAME_ALREADY_EXISTS", "The username entered already exists");
define("EMAIL_EMPTY", "The email field was empty.");
define("EMAIL_INVALID", "The email address entered is invalid.");
define("EMAIL_ALREADY_EXISTS", "The email entered already exists");
define("PASSWORD_EMPTY", "The password field was empty.");
define("PASSWORD_INVALID", "The password entered was invalid.  Passwords must be atleast 6 characters long.");
define("PASSWORD_RETYPE_EMPTY", "The re-entered password field is empty");
define("PASSWORDS_DONT_MATCH", "The passwords entered do not match");
define("LOGIN_FAILED", "The username or password that was entered is incorrect or doesn't exist.");
define("LOGIN_FAILED_TOOMANY", "You have failed to login too many times in a row.  Please try again in 30 seconds.");
define("ACCOUNT_NOT_VERIFIED", "This account has not been verified.  Please contact the administrator to resolve this.");


define("LOGGED_IN", "You are now logged in");
define("LOGGED_OUT", "You are now logged out");
define("SESSION_TIMEOUT_MESSAGE", "Your session has expired.  Please log in again.");

define("REGISTRATION_SUCCESSFULL", "Your registration was successful.");
define("REGISTRATION_FAILED", "Your registration has failed.");

define("NAME_INVALID", "Names can only contain alphanumeric characters");
define("PROFILE_UPDATE_SUCCESS", "Your profile information has been saved.");
define("PROFILE_UPDATE_FAILED", "There was an error when updating your profile");
define("MISSING_REQUIRED", "Plese fill in all required fields");
define("PASSWORD_WRONG", "The old password entered was incorrect.");
define("PASSWORD_SAME_AS_OLD", "The new password is the same as the old.");
define("PASSWORD_CHANGED", "You password was successfully changed.");
define("PASSWORD_CHANGE_FAILED", "There was an error while trying to change your password.");

define("ACCOUNT_INFO_SAME", "The account info is the same as before.");
define("ACCOUNT_INFO_CHANGED", "You account info has been updated.");
define("ACCOUNT_INFO_CHANGE_FAILED", "There was an error while trying to change your account info.");

define("DATE_MISSING", "The date field was empty.");
define("COMP_NAME_MISSING", "The competition name field was empty.");
define("COMP_LOCATION_MISSING", "The competition location field was empty.");
define("COMP_LEVELS_MISSING", "The competition levels field was empty.");

define("INVALID_DATA", "There was a problem with the data submitted.");
define("RECORD_DOES_NOT_EXIST", "The record requested does not exist");
?>