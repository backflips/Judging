<?php
require('fpdf.php');

class PDF extends FPDF{
    var $widths;
    var $aligns;
    var $defaultWidths = array(11,11,51,29,36.3975,12,16,13,13,14,16,37);
    
    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r", '', $txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
    
    function setWidths($w) {
        $this->widths = $w;
    }
    
    function generateTableHeader($name, $reg, $province, $rating, $chair) {
        $this->SetFont('times','',12);
        $this->SetDrawColor(0,0,0);
        $this->setFillColor(200,200,200);
        $userHeadings = array("Name", "Registration #", "Province", "Rating", "Judging Chairperson");
        $userHeadingWidth = array(61.3975, 39, 29, 45, 85);
        $tableHeadings = array("Date", "Competition", "City", "Level", "Floor", "Pommel", "Rings", "Vault", "Pbars", "Highbar", "Head Judge");
        $tableHeadingWidth = array(22,51,29,36.3975,12,16,13,13,14,16,37);
        $tableHeadings2 = array("MM", "DD", "", "", "", "", "", "", "", "", "", "");
        $userInfo = array($name, $reg, $province, $rating, $chair);
        
        $this->setWidths($userHeadingWidth);
        for($i = 0; $i < count($userHeadings); $i++) {
            $this->Cell($this->widths[$i], 5, $userHeadings[$i], 'RTL', 0, 'L',1);
        }
        $this->Ln();
        
        $this->SetFont('times','B',14);
        
        $nb=0;
        for($i=0;$i<count($userInfo);$i++)
            $nb=max($nb, $this->NbLines($this->widths[$i], $userInfo[$i]));
        
        $h=7*$nb;
        for($i=0;$i<count($userInfo);$i++) {
            $w=$this->widths[$i];
            $a= 'C';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x, $y, $w, $h, 'F');
            //Print the text
            $this->MultiCell($w, $h, $userInfo[$i], 'LBR', $a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w, $y);
        }
        //Go to the next line
        $this->Ln($h);
        $this->SetFont('times','',12);
        
        $this->setWidths($tableHeadingWidth);
        for($i = 0; $i < count($tableHeadings); $i++) {
            $this->Cell($this->widths[$i], 5, $tableHeadings[$i], 'RTL', 0, 'L',1);
        }
        $this->Ln();
        $this->setWidths($this->defaultWidths);
        for($i = 0; $i < count($tableHeadings2); $i++) {
            $this->Cell($this->widths[$i], 4, $tableHeadings2[$i], 'RBL', 0, 'C',1);
        }
        $this->Ln();
    }

    function Header() {      
        
        $start = date('F d, Y', strtotime($_POST['start']));
        $end = date('F d, Y', strtotime($_POST['end']));
        $title = "GCG Men's National Judging Record";
        
        $this->Image('includes/GCG-Logo.jpg', 25, 9, 24);
        $this->SetFont('times','BI',24);
        $w = $this->GetStringWidth($title)+60;
        $this->SetX(((279-$w)/2)+20);
        $this->SetFillColor(215,210,187);
        $this->SetTextColor(0,0,0);
        $this->Cell($w,12,$title,0,1,'C',true);
        $this->SetX(((279-$w)/2)+20);
        $this->SetFont('times','Bi',16);
        $this->Cell($w,6,$start. " - " .$end,0,1,'C',true);
    // Line break
        $this->Ln(15);
    }
    function addRecord($r) {
        $this->SetFont('times','',12);
        $this->SetDrawColor(0,0,0);
        $this->setFillColor(255,255,255);
        $data = array(date('m',strtotime($r->record_date)), date('d',strtotime($r->record_date)), $r->competition_name);
        $this->setWidths(array(11,11,51));
        
        for($i = 0; $i< 3; $i++) {
            $this->Cell($this->widths[$i],6,$data[$i],1,0,'L',true);
        }
    }
}

//$login = new Profile();
$userRecords = $record->getUserRecords($_SESSION['user_id'], $_POST['start'], $_POST['end']);

$pdf = new PDF('L', 'mm', 'letter');
$pdf->AddPage();
$pdf->SetFont('times','BI',24);
$pdf->generateTableHeader($login->getUserFirstName() . " " . $login->getUserLastName(),"000000","BC","National","");
$pdf->addRecord($userRecords[0]);
$pdf->AddPage();
$pdf->Output();


$records = $record->getUserRecords($_SESSION['user_id'], (isset($_POST['start']) && validateDate($_POST['start'])) ? $_POST['start']: date('Y-m-d',strtotime("-1 year")) , (isset($_POST['start']) && validateDate($_POST['end'])) ? $_POST['end']: date('Y-m-d'));


?>