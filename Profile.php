<?php
require_once('Login.php');
    class Category {
        public $cat_name;
        public $disc_name;
    }

    class Profile extends Login{
        
        private $user_profile = null;
        
        private $user_certifications = null;
        
        public function __construct() {
            parent::__construct();
            
            if($_SESSION['user_logged_in'] == 1) {
                $this->user_profile = $this->getUserProfile($_SESSION['user_id']);
                $this->user_certifications = $this->setUserCerts($_SESSION['user_id']);
                if(isset($_POST['save_profile'])) {
                    $this->editProfile((isset($_POST['firstName']) ? $_POST['firstName'] : ""), (isset($_POST['lastName']) ? $_POST['lastName'] : ""), (isset($_POST['phoneHome']) ? $_POST['phoneHome'] : ""), (isset($_POST['phoneCell']) ? $_POST['phoneCell'] : ""), (isset($_POST['phoneWork']) ? $_POST['phoneWork'] : ""));
                }
            }
        }
        
        private function getUserProfile($userid){
            if($this->databaseConnect()) {
                $query = $this->db_connection->prepare('SELECT * FROM user_profile WHERE user_id= :userid');
                $query->bindValue(':userid', $userid, PDO::PARAM_STR);
                $query->execute();
                return $query->fetchObject();
            }
            else {
                return false;
            }
        }
        
        private function setUserCerts($userid) {
            if($this->databaseConnect()) {
                $query = $this->db_connection->prepare('SELECT discipline_name, category_name FROM categories WHERE user_id= :userid');
                $query->bindValue(':userid', $userid, PDO::PARAM_STR);
                $query->execute();
                return $query->fetchAll(PDO::FETCH_CLASS, "Category");
            }
            else {
                return false;
            }
        }
        
        public function getUserCerts() {
            return $this->user_certifications;
        }
        
        private function editProfile($firstname, $lastname, $phoneHome, $phoneCell, $phoneWork) {
            $firstname =  filter_var($firstname, FILTER_SANITIZE_STRING);
            $lastname = filter_var($lastname, FILTER_SANITIZE_STRING);
            $phoneHome = filter_var($phoneHome, FILTER_SANITIZE_STRING);
            $phoneCell = filter_var($phoneCell, FILTER_SANITIZE_STRING);
            $phoneWork = filter_var($phoneWork, FILTER_SANITIZE_STRING);
            
            $query = $this->db_connection->prepare('UPDATE user_profile SET userp_firstname = :firstname, userp_lastname = :lastname, userp_phone_home = :phoneHome,
                                                   userp_phone_cell = :phoneCell, userp_phone_work = :phoneWork WHERE user_id = :user_id');
            $query->bindValue(':firstname', $firstname, PDO::PARAM_STR);
            $query->bindValue(':lastname', $lastname, PDO::PARAM_STR);
            $query->bindValue(':phoneHome', $phoneHome, PDO::PARAM_STR);
            $query->bindValue(':phoneCell', $phoneCell, PDO::PARAM_STR);
            $query->bindValue(':phoneWork', $phoneWork, PDO::PARAM_STR);
            $query->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_STR);
            $query->execute();
            
            if($query->rowCount() <= 1) {
                $this->messages[] = PROFILE_UPDATE_SUCCESS;
            }
            else {
                $this->errors[] = PROFILE_UPDATE_FAILED;
            }
            $this->user_profile = $this->getUserProfile($_SESSION['user_id']);
        }
        
        public function getUserFirstName() {
            if(isset($this->user_profile->userp_firstname)) {
                return $this->user_profile->userp_firstname;
            }
            else {
                return "";
            }
        }
        
        public function getUserLastName() {
            if(isset($this->user_profile->userp_lastname)) {
                return $this->user_profile->userp_lastname;
            }
            else {
                return "";
            }
        }
        
        public function getUserPhoneHome() {
            if(isset($this->user_profile->userp_phone_home)) {
                return $this->user_profile->userp_phone_home;
            }
            else {
                return "";
            }
        }
        public function getUserPhoneCell() {
            if(isset($this->user_profile->userp_phone_cell)) {
                return $this->user_profile->userp_phone_cell;
            }
            else {
                return "";
            }
        }
        public function getUserPhoneWork() {
            if(isset($this->user_profile->userp_phone_work)) {
                return $this->user_profile->userp_phone_work;
            }
            else {
                return "";
            }
        }
        public function getUserEmail() {
            if(isset($this->user_profile->userp_email)) {
                return $this->user_profile->userp_email;
            }
            else {
                return "";
            }
        }
    }
    
?>

