INSERT INTO user_login(user_name, user_password_hash, user_email, user_verified) VALUES ("test", "$2y$10$hckQuu5cY3aMbar8w6C.VeWr0uaaDkfKMM5dFYsYXMpBcTdzHDwSG", "test@test.com", 1), ("test2", "$2y$10$mS5kjp7rq6TOFFN00cS5KeRPLDKmNgRbuj2x8lFbRtmwG66EwrCh.", "test2@test.com", 1);

INSERT INTO discipline(discipline_name) VALUES ("MAG"), ("WAG");

INSERT INTO category(category_name, discipline_id) VALUES ("FIG", 1), ("National", 1), ("Provincial", 1);

INSERT INTO user_category(user_id, category_id, discipline_id) VALUES (1, 2, 1), (2, 3, 1);

INSERT INTO season(season_name, season_start, season_end) VALUES ("2013/2014", "2013-09-01","2014-08-31"), ("2014/2015", "2014-09-01","2015-08-31"), ("2015/2016", "2014-09-01","2015-08-31");

INSERT INTO record(record_date, competition_name, competition_city, competition_levels, floor_optional, rings_optional, head_judge) VALUES ("2014-11-11", "TEST", "testville", "1, 2, 3", 1, 1, "TEST TESTERSON");

INSERT INTO user_record(record_id, user_id) VALUES (1,1);