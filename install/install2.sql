CREATE DATABASE IF NOT EXISTS tracking;

CREATE TABLE IF NOT EXISTS tracking.user_login (
    user_id INT NOT NULL AUTO_INCREMENT COMMENT 'a unique index for users',
    user_name VARCHAR(64) NOT NULL COMMENT 'a unique username',
    user_password_hash VARCHAR(255) NOT NULL COMMENT 'the users salted and hashed password',
	user_email VARCHAR(255) NOT NULL COMMENT 'a users unique email',
	user_verified TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'has the user been verified',
	user_rememberme_token VARCHAR(64) DEFAULT NULL COMMENT 'a users remember-me token',
	user_failed_logins TINYINT(1) NOT NULL DEFAULT 0 COMMENT '# of failed login attempts',
    user_last_failed_login INT(10) DEFAULT NULL COMMENT 'unix timestamp of last failed attempt',
	user_registration_datetime DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	user_registration_ip varchar(39) NOT NULL DEFAULT '0.0.0.0',
	PRIMARY KEY(user_id),
	UNIQUE KEY (user_name),
	UNIQUE KEY (user_email)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='users login data';

CREATE TABLE IF NOT EXISTS tracking.user_profile (
    userp_id INT NOT NULL AUTO_INCREMENT COMMENT 'unique index for user profile',
    user_id INT NOT NULL COMMENT 'Users login id',
    userp_firstname VARCHAR(255) COMMENT 'Users first name',
    userp_lastname VARCHAR(255) COMMENT 'Users last name',
    userp_phone_home VARCHAR(20) COMMENT 'Users home phone number',
    userp_phone_cell VARCHAR(20) COMMENT 'Users cell phone number',
    userp_phone_work VARCHAR(20) COMMENT 'Users work phone number',
    userp_email VARCHAR(255) COMMENT 'Users contact email',
    PRIMARY KEY(userp_id),
    UNIQUE KEY(user_id),
    FOREIGN KEY(user_id) REFERENCES tracking.user_login(user_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='users profile data';

CREATE TABLE IF NOT EXISTS tracking.discipline (
    discipline_id INT NOT NULL AUTO_INCREMENT COMMENT 'unique index for discipline',
    discipline_name VARCHAR(255) NOT NULL COMMENT 'the disciplines name',
    PRIMARY KEY(discipline_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='a discipline';

CREATE TABLE IF NOT EXISTS tracking.category (
    category_id INT NOT NULL AUTO_INCREMENT COMMENT 'unique index for category',
    discipline_id INT NOT NULL COMMENT 'discipline id',
    category_name VARCHAR(255) NOT NULL COMMENT 'the categories name',
    PRIMARY KEY(category_id),
    FOREIGN KEY(discipline_id) REFERENCES tracking.discipline(discipline_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='a category level';

CREATE TABLE IF NOT EXISTS tracking.user_category (
    user_category_id INT NOT NULL AUTO_INCREMENT COMMENT 'unique index for user category',
    user_id INT NOT NULL COMMENT 'users id',
    category_id INT NOT NULL COMMENT 'the categories ID',
    discipline_id INT NOT NULL COMMENT 'the categories discipline',
    PRIMARY KEY(user_category_id),
    UNIQUE KEY(user_id,discipline_id),
    FOREIGN KEY(user_id) REFERENCES tracking.user_profile(user_id),
    FOREIGN KEY(category_id) REFERENCES tracking.category(category_id),
    FOREIGN KEY(discipline_id) REFERENCES tracking.discipline(discipline_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='users category';

CREATE TABLE IF NOT EXISTS tracking.season (
    season_id INT NOT NULL AUTO_INCREMENT COMMENT 'unique index for competition season',
    season_name VARCHAR(255) NOT NULL COMMENT 'the seasons name',
    season_start DATE NOT NULL,
    season_end DATE NOT NULL,
    PRIMARY KEY(season_id),
    UNIQUE KEY(season_name)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='a season';

CREATE TABLE IF NOT EXISTS tracking.record (
    record_id INT NOT NULL AUTO_INCREMENT COMMENT 'unique index for record',
    record_date DATE NOT NULL COMMENT 'date of competition',
    competition_name VARCHAR(255) NOT NULL COMMENT 'name of competition',
    competition_city VARCHAR(255) NOT NULL COMMENT 'Location of competition',
    competition_levels VARCHAR(255) NOT NULL COMMENT 'Levels of competition',
    floor_compulsory TINYINT(1) NOT NULL DEFAULT 0,
    floor_optional TINYINT(1) NOT NULL DEFAULT 0,
    floor_finals TINYINT(1) NOT NULL DEFAULT 0,
    pommel_compulsory TINYINT(1) NOT NULL DEFAULT 0,
    pommel_optional TINYINT(1) NOT NULL DEFAULT 0,
    pommel_finals TINYINT(1) NOT NULL DEFAULT 0,
    rings_compulsory TINYINT(1) NOT NULL DEFAULT 0,
    rings_optional TINYINT(1) NOT NULL DEFAULT 0,
    rings_finals TINYINT(1) NOT NULL DEFAULT 0,
    vault_compulsory TINYINT(1) NOT NULL DEFAULT 0,
    vault_optional TINYINT(1) NOT NULL DEFAULT 0,
    vault_finals TINYINT(1) NOT NULL DEFAULT 0,
    pbars_compulsory TINYINT(1) NOT NULL DEFAULT 0,
    pbars_optional TINYINT(1) NOT NULL DEFAULT 0,
    pbars_finals TINYINT(1) NOT NULL DEFAULT 0,
    hbar_compulsory TINYINT(1) NOT NULL DEFAULT 0,
    hbar_optional TINYINT(1) NOT NULL DEFAULT 0,
    hbar_finals TINYINT(1) NOT NULL DEFAULT 0,
    head_judge VARCHAR(255) NOT NULL,
    PRIMARY KEY(record_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='a single record';

CREATE TABLE IF NOT EXISTS tracking.user_record (
    record_id INT NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY(record_id, user_id),
    FOREIGN KEY(record_id) REFERENCES tracking.record(record_id) ON DELETE CASCADE,
    FOREIGN KEY(user_id) REFERENCES tracking.user_profile(user_id) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='a user record';

use tracking;

CREATE VIEW tracking.categories AS
    SELECT uc.user_category_id, uc.user_id, uc.category_id, d.discipline_name, c.category_name
    FROM user_category as uc 
	JOIN category as c ON (c.category_id = uc.category_id)
    JOIN discipline as d ON (d.discipline_id = uc.discipline_id);
    
CREATE VIEW tracking.user_records AS
    SELECT ur.user_id, r.*
    FROM user_record as ur
        JOIN record as r ON (ur.record_id = r.record_id);


delimiter //

CREATE PROCEDURE insert_record(IN userid INT, IN record_date DATE, IN name VARCHAR(255), IN location VARCHAR(255), IN levels VARCHAR(255), IN floorC TINYINT(1), IN floorO TINYINT(1),IN floorF TINYINT(1), IN pommelC TINYINT(1), IN pommelO TINYINT(1),IN pommelF TINYINT(1), IN ringsC TINYINT(1), IN ringsO TINYINT(1),IN ringsF TINYINT(1), IN vaultC TINYINT(1), IN vaultO TINYINT(1),IN vaultF TINYINT(1), IN pbarsC TINYINT(1), IN pbarsO TINYINT(1),IN pbarsF TINYINT(1), IN hbarC TINYINT(1), IN hbarO TINYINT(1),IN hbarF TINYINT(1), IN hJudge VARCHAR(255))
BEGIN
    INSERT INTO record(record_date, competition_name, competition_city, competition_levels, floor_compulsory, floor_optional, floor_finals, pommel_compulsory, pommel_optional, pommel_finals, rings_compulsory, rings_optional, rings_finals, vault_compulsory, vault_optional, vault_finals, pbars_compulsory, pbars_optional, pbars_finals, hbar_compulsory, hbar_optional, hbar_finals, head_judge)
        VALUES (record_Date, name, location, levels, floorC, floorO, floorF, pommelC, pommelO, pommelF, ringsC, ringsO, ringsF, vaultC, vaultO, vaultF, pbarsC, pbarsO, pbarsF, hbarC, hbarO, hbarF, hJudge);
    INSERT INTO user_record(user_id, record_id) VALUES (userid, LAST_INSERT_ID());
END//

CREATE TRIGGER ins_prof AFTER INSERT ON user_login
FOR EACH ROW
INSERT INTO user_profile(user_id, userp_email) VALUES (NEW.user_id, NEW.user_email);//


CREATE TRIGGER upd_email AFTER UPDATE ON user_login
FOR EACH ROW
UPDATE user_profile SET userp_email = NEW.user_email;//


delimiter ;