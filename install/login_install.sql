CREATE DATABASE IF NOT EXISTS tracking_login;

CREATE TABLE IF NOT EXISTS tracking_login.users (
	user_id INT NOT NULL AUTO_INCREMENT COMMENT 'a unique index for users',
	user_name VARCHAR(64) NOT NULL COMMENT 'a unique username',
	user_password_hash VARCHAR(255) NOT NULL COMMENT 'the users salted and hashed password',
	user_email VARCHAR(255) NOT NULL COMMENT 'a users unique email',
	user_verified TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'has the user been verified',
	user_rememberme_token VARCHAR(64) DEFAULT NULL COMMENT 'a users remember-me token',
	user_failed_logins TINYINT(1) NOT NULL DEFAULT 0 COMMENT '# of failed login attempts',
    user_last_failed_login INT(10) DEFAULT NULL COMMENT 'unix timestamp of last failed attempt',
	user_registration_datetime DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	user_registration_ip varchar(39) NOT NULL DEFAULT '0.0.0.0',
	PRIMARY KEY(user_id),
	UNIQUE KEY (user_name),
	UNIQUE KEY (user_email)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='users login data';