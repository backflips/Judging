<div class="panel panel-default">
    <div class="panel-heading"><strong>Profile Information</strong></div>
    <div class="panel-body">
        <form method="post" action="settings.php?profile" name="profileform" class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-xs-2">First Name</label>
                <p class="form-control-static col-xs-10"><?php echo $login->getUserFirstName() ?></p>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-2">Last Name</label>
                <p class="form-control-static col-xs-10"><?php echo $login->getUserLastName() ?></p>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-2">Home Phone Number</label>
                <p class="form-control-static col-xs-10"><?php echo $login->getUserPhoneHome() ?></p>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-2">Cell Phone Number</label>
                <p class="form-control-static col-xs-10"><?php echo $login->getUserPhoneCell() ?></p>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-2">Work Phone Number</label>
                <p class="form-control-static col-xs-10"><?php echo $login->getUserPhoneWork() ?></p>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-2">Email</label>
                <p class="form-control-static col-xs-10"><?php echo $login->getUserEmail() ?></p>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-3 col-md-offset-2">
                <button type="submit" class="btn btn-primary btn-block" name="edit_profile">Edit</button>
            </div>
        </form>
    </div>
</div>
