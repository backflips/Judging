<div class="panel panel-default">
     <div class="panel-heading"><strong>Profile Information</strong></div>
     <div class="panel-body">
         <form method="post" action="settings.php?profile" name="profileform" class="form-horizontal">
             <div class="form-group">
                 <label class="control-label col-sm-2">First Name</label>
                 <div class="col-sm-8">
                     <input type="text" class="form-control" id="inputFirstName" name="firstName" value="<?php echo $login->getUserFirstName() ?>">
                 </div>
             </div>
             <div class="form-group">
                 <label class="control-label col-sm-2">Last Name</label>
                 <div class="col-sm-8">
                     <input type="text" class="form-control" id="inputLastName" name="lastName" value="<?php echo $login->getUserLastName() ?>">
                 </div>
             </div>
             <div class="form-group">
                 <label class="control-label col-sm-2">Home Phone Number</label>
                 <div class="col-sm-8">
                     <input type="text" class="form-control" id="inputPhoneHome" name="phoneHome" value="<?php echo $login->getUserPhoneHome() ?>">
                 </div>
             </div>
             <div class="form-group">
                 <label class="control-label col-sm-2">Cell Phone Number</label>
                 <div class="col-sm-8">
                     <input type="text" class="form-control" id="inputPhoneCell" name="phoneCell" value="<?php echo $login->getUserPhoneCell() ?>">
                 </div>
             </div>
             <div class="form-group">
                 <label class="control-label col-sm-2">Work Phone Number</label>
                 <div class="col-sm-8">
                     <input type="text" class="form-control" id="inputPhoneWork" name="phoneWork" value="<?php echo $login->getUserPhoneWork() ?>">
                 </div>
             </div>
             <div class="form-group">
                 <label class="control-label col-sm-2">Email</label>
                 <div class="col-sm-8">
                 <p class="form-control-static"><?php echo $login->getUserEmail() ?></p>
                 </div>
             </div>
             <div class="col-xs-6 col-sm-5 col-sm-offset-1 col-md-3 col-md-offset-2">
                 <button type="submit" class="btn btn-success btn-block" name="save_profile">Save</button>
             </div>
             <div class="col-xs-6 col-sm-5 col-md-3">
                 <a class="btn btn-warning btn-block" href="settings.php?profile">Cancel</a>
             </div>
         </form>
     </div>
 </div>
