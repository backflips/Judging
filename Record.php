<?php
class Record {
    public $user_id;
    public $record_id;
    public $record_date;
    public $competition_name;
    public $competition_city;
    public $competition_levels;
    public $floor_compulsory;
    public $floor_optional;
    public $floor_finals;
    public $pommel_compulsory;
    public $pommel_optional;
    public $pommel_finals;
    public $rings_compulsory;
    public $rings_optional;
    public $rings_finals;
    public $vault_compulsory;
    public $vault_optional;
    public $vault_finals;
    public $pbars_compulsory;
    public $pbars_optional;
    public $pbars_finals;
    public $hbar_compulsory;
    public $hbar_optional;
    public $hbar_finals;
    public $head_judge;
}

class userRecord {
    public $messages = array();
    
    public $errors = array();
    
    private $user_seasons;
    
    private $db_connection = null;
    
    public function __construct() {
        require_once('config.php');
        require_once('includes/messages.php');
        
        if(isset($_POST['save_record'])) {
            $this->saveRecord();
        }
        else if(isset($_POST['delete_record'])){
            $this->delete_record();
        }
        else if(isset($_POST['save_edit_record'])) {
            $this->edit_record();
        }
        
    }
    
    private function databaseConnect() {
        if ($this->db_connection != null) {
            return true;
        }
        else {
            try {
                $this->db_connection = new PDO('mysql:host='. DB_HOST . ';dbname='. DB_NAME . ';charset=utf8', DB_USER, DB_PASS);
                return true;
            } catch(PDOException $e) {
                $this->errors[] = DATABASE_ERROR . $e->getMessage();
            }
        }
        return false;
    }
    
    public function getUserRecords($user, $start, $end) {
        if ($this->databaseConnect()) {
            $query = $this->db_connection->prepare('SELECT * FROM user_records WHERE user_id = :user_id AND record_date >= :start AND record_date <= :end ORDER BY record_date');
            $query->bindValue(':user_id', $user, PDO::PARAM_STR);
            $query->bindValue(':start', $start, PDO::PARAM_STR);
            $query->bindValue(':end', $end, PDO::PARAM_STR);
            $query->execute();
            
            return $query->fetchAll(PDO::FETCH_CLASS, "Record");
        }
        else {
            return false;
        }
    }
    
    public function getRecord($record_id) {
        if ($this->databaseConnect()) {
            $query = $this->db_connection->prepare('SELECT * FROM user_records WHERE record_id = :record_id');
            $query->bindValue(':record_id', $record_id, PDO::PARAM_STR);
            $query->execute();
            
            return $query->fetchObject();
        }
        else {
            return false;
        }
    }
    
    public function getAllSeasons() {
        if ($this->databaseConnect()) {
                $query = $this->db_connection->prepare('SELECT * FROM season');
                $query->execute();
                return $query->fetchAll(PDO::FETCH_CLASS, "Season");
            }
            else {
                return false;
            }
    }
    
    private function saveRecord() {
        $_GET['add_record'] = true;
        $user = $_SESSION['user_id'];
        $date = (isset($_POST['date'])) ? filter_var($_POST['date'], FILTER_SANITIZE_STRING) : "";
        $name = (isset($_POST['name'])) ? filter_var($_POST['name'], FILTER_SANITIZE_STRING) : "";
        $location = (isset($_POST['location'])) ? filter_var($_POST['location'], FILTER_SANITIZE_STRING) : "";
        $levels  = (isset($_POST['levels'])) ? filter_var($_POST['levels'], FILTER_SANITIZE_STRING) : "";
        $headJudge = (isset($_POST['headJudge'])) ? filter_var($_POST['headJudge'], FILTER_SANITIZE_STRING) : "";
        $floorComp = (isset($_POST['floorComp'])) ? true : 0;
        $floorOpt = (isset($_POST['floorOpt'])) ? true : 0;
        $floorFin = (isset($_POST['floorFin'])) ? true : 0;
        $pommelComp = (isset($_POST['pommelComp'])) ? true : 0;
        $pommelOpt = (isset($_POST['pommelOpt'])) ? true : 0;
        $pommelFin = (isset($_POST['pommelFin'])) ? true : 0;
        $ringsComp = (isset($_POST['ringsComp'])) ? true : 0;
        $ringsOpt = (isset($_POST['ringsOpt'])) ? true : 0;
        $ringsFin = (isset($_POST['ringsFin'])) ? true : 0;
        $vaultComp = (isset($_POST['vaultComp'])) ? true : 0;
        $vaultOpt = (isset($_POST['vaultOpt'])) ? true : 0;
        $vaultFin = (isset($_POST['vaultFin'])) ? true : 0;
        $pbarsComp = (isset($_POST['pbarsComp'])) ? true : 0;
        $pbarsOpt = (isset($_POST['pbarsOpt'])) ? true : 0;
        $pbarsFin = (isset($_POST['pbarsFin'])) ? true : 0;
        $hbarComp = (isset($_POST['hbarComp'])) ? true : 0;
        $hbarOpt = (isset($_POST['hbarOpt'])) ? true : 0;
        $hbarFin = (isset($_POST['hbarFin'])) ? true : 0;
        
        if(empty($date)) {
            $this->errors[] = DATE_MISSING;
        }
        else if(empty($name)) {
            $this->errors[] = COMP_NAME_MISSING;
        }
         else if(empty($location)) {
            $this->errors[] = COMP_LOCATION_MISSING;
        }
         else if(empty($levels)) {
            $this->errors[] = COMP_LEVELS_MISSING;
        }
        else{
            if($this->databaseConnect()) {
                $query = $this->db_connection->prepare('CALL insert_record(:user_id, :date, :name, :location, :levels,
                                                       :floorC, :floorO, :floorF,
                                                       :pommelC, :pommelO, :pommelF,
                                                       :ringsC, :ringsO, :ringsF,
                                                       :vaultC, :vaultO, :vaultF,
                                                       :pbarsC, :pbarsO, :pbarsF,
                                                       :hbarC, :hbarO, :hbarF,                                                       
                                                       :headJudge)');
                $query->bindValue(':user_id', $user, PDO::PARAM_STR);
                $query->bindValue(':date', $date, PDO::PARAM_STR);
                $query->bindValue(':name', $name, PDO::PARAM_STR);
                $query->bindValue(':location', $location, PDO::PARAM_STR);
                $query->bindValue(':levels', $levels, PDO::PARAM_STR);
                $query->bindValue(':floorC', $floorComp, PDO::PARAM_STR);
                $query->bindValue(':floorO', $floorOpt, PDO::PARAM_STR);
                $query->bindValue(':floorF', $floorFin, PDO::PARAM_STR);
                $query->bindValue(':pommelC', $pommelComp, PDO::PARAM_STR);
                $query->bindValue(':pommelO', $pommelOpt, PDO::PARAM_STR);
                $query->bindValue(':pommelF', $pommelFin, PDO::PARAM_STR);
                $query->bindValue(':ringsC', $ringsComp, PDO::PARAM_STR);
                $query->bindValue(':ringsO', $ringsOpt, PDO::PARAM_STR);
                $query->bindValue(':ringsF', $ringsFin, PDO::PARAM_STR);
                $query->bindValue(':vaultC', $vaultComp, PDO::PARAM_STR);
                $query->bindValue(':vaultO', $vaultOpt, PDO::PARAM_STR);
                $query->bindValue(':vaultF', $vaultFin, PDO::PARAM_STR);
                $query->bindValue(':pbarsC', $pbarsComp, PDO::PARAM_STR);
                $query->bindValue(':pbarsO', $pbarsOpt, PDO::PARAM_STR);
                $query->bindValue(':pbarsF', $pbarsFin, PDO::PARAM_STR);
                $query->bindValue(':hbarC', $hbarComp, PDO::PARAM_STR);
                $query->bindValue(':hbarO', $hbarOpt, PDO::PARAM_STR);
                $query->bindValue(':hbarF', $hbarFin, PDO::PARAM_STR);
                $query->bindValue(':headJudge', $headJudge, PDO::PARAM_STR);
                $query->execute();
                unset($_GET['add_record']);
            }
        }
    }
    
    private function delete_record() {
        $record_id = (isset($_POST['delete_record'])) ? filter_var(filter_var($_POST['delete_record'], FILTER_SANITIZE_STRING), FILTER_VALIDATE_INT) : "";
        
        if(empty($record_id)) {
            $this->errors[] = DATABASE_ERROR;
        }
        else if(!isset($this->getRecord($record_id)->user_id)) {
            $this->errors[] = RECORD_DOES_NOT_EXIST;
        }
        else if($this->getRecord($record_id)->user_id != $_SESSION['user_id']) {
            $this->errors[] = INVALID_DATA;
        }
        else {
            if ($this->databaseConnect()) {
                $query = $this->db_connection->prepare('DELETE FROM record WHERE record_id = :record_id');
                $query->bindValue(':record_id', $record_id, PDO::PARAM_STR);
                $query->execute();
            }
            else {
                return false;
            }
        }
    }
    
    private function edit_record() {
        
        $record_id = (isset($_POST['save_edit_record'])) ? filter_var(filter_var($_POST['save_edit_record'], FILTER_SANITIZE_STRING), FILTER_VALIDATE_INT) : "";
        $_POST['edit_record'] = (isset($_POST['save_edit_record'])) ? filter_var(filter_var($_POST['save_edit_record'], FILTER_SANITIZE_STRING), FILTER_VALIDATE_INT) : "";
        
        $user = $_SESSION['user_id'];
        
        $date = (isset($_POST['date'])) ? filter_var($_POST['date'], FILTER_SANITIZE_STRING) : "";
        $name = (isset($_POST['name'])) ? filter_var($_POST['name'], FILTER_SANITIZE_STRING) : "";
        $location = (isset($_POST['location'])) ? filter_var($_POST['location'], FILTER_SANITIZE_STRING) : "";
        $levels  = (isset($_POST['levels'])) ? filter_var($_POST['levels'], FILTER_SANITIZE_STRING) : "";
        $headJudge = (isset($_POST['headJudge'])) ? filter_var($_POST['headJudge'], FILTER_SANITIZE_STRING) : "";
        $floorComp = (isset($_POST['floorComp'])) ? true : 0;
        $floorOpt = (isset($_POST['floorOpt'])) ? true : 0;
        $floorFin = (isset($_POST['floorFin'])) ? true : 0;
        $pommelComp = (isset($_POST['pommelComp'])) ? true : 0;
        $pommelOpt = (isset($_POST['pommelOpt'])) ? true : 0;
        $pommelFin = (isset($_POST['pommelFin'])) ? true : 0;
        $ringsComp = (isset($_POST['ringsComp'])) ? true : 0;
        $ringsOpt = (isset($_POST['ringsOpt'])) ? true : 0;
        $ringsFin = (isset($_POST['ringsFin'])) ? true : 0;
        $vaultComp = (isset($_POST['vaultComp'])) ? true : 0;
        $vaultOpt = (isset($_POST['vaultOpt'])) ? true : 0;
        $vaultFin = (isset($_POST['vaultFin'])) ? true : 0;
        $pbarsComp = (isset($_POST['pbarsComp'])) ? true : 0;
        $pbarsOpt = (isset($_POST['pbarsOpt'])) ? true : 0;
        $pbarsFin = (isset($_POST['pbarsFin'])) ? true : 0;
        $hbarComp = (isset($_POST['hbarComp'])) ? true : 0;
        $hbarOpt = (isset($_POST['hbarOpt'])) ? true : 0;
        $hbarFin = (isset($_POST['hbarFin'])) ? true : 0;
        
        if(empty($record_id)) {
            $this->errors[] = DATABASE_ERROR;
        }
        else if(!isset($this->getRecord($record_id)->user_id)) {
            $this->errors[] = RECORD_DOES_NOT_EXIST;
        }
        else if($this->getRecord($record_id)->user_id != $user) {
            $this->errors[] = INVALID_DATA;
        }
        else if(empty($date)) {
            $this->errors[] = DATE_MISSING;
        }
        else if(empty($name)) {
            $this->errors[] = COMP_NAME_MISSING;
        }
        else if(empty($location)) {
            $this->errors[] = COMP_LOCATION_MISSING;
        }
        else if(empty($levels)) {
            $this->errors[] = COMP_LEVELS_MISSING;
        }
        else {
            if($this->databaseConnect()) {
                $query = $this->db_connection->prepare('UPDATE record SET   record_date         =   :date,
                                                                            competition_name    =   :name,
                                                                            competition_city    =   :location,
                                                                            competition_levels  =   :levels,
                                                                            floor_compulsory    =   :floorComp,
                                                                            floor_optional      =   :floorOpt,
                                                                            floor_finals        =   :floorFin,
                                                                            pommel_compulsory   =   :pommelComp,
                                                                            pommel_optional     =   :pommelOpt,
                                                                            pommel_finals       =   :pommelFin,
                                                                            rings_compulsory    =   :ringsComp,
                                                                            rings_optional      =   :ringsOpt,
                                                                            rings_finals        =   :ringsFin,
                                                                            vault_compulsory    =   :vaultComp,
                                                                            vault_optional      =   :vaultOpt,
                                                                            vault_finals        =   :vaultFin,
                                                                            pbars_compulsory    =   :pbarsComp,
                                                                            pbars_optional      =   :pbarsOpt,
                                                                            pbars_finals        =   :pbarsFin,
                                                                            hbar_compulsory     =   :hbarComp,
                                                                            hbar_optional       =   :hbarOpt,
                                                                            hbar_finals         =   :hbarFin,
                                                                            head_judge          =   :headJudge
                                                        WHERE record_id = :record_id'
                                                                            );
                $query->bindValue(':date', $date, PDO::PARAM_STR);
                $query->bindValue(':name', $name, PDO::PARAM_STR);
                $query->bindValue(':location', $location, PDO::PARAM_STR);
                $query->bindValue(':levels', $levels, PDO::PARAM_STR);
                $query->bindValue(':floorComp', $floorComp, PDO::PARAM_STR);
                $query->bindValue(':floorOpt', $floorOpt, PDO::PARAM_STR);
                $query->bindValue(':floorFin', $floorFin, PDO::PARAM_STR);
                $query->bindValue(':pommelComp', $pommelComp, PDO::PARAM_STR);
                $query->bindValue(':pommelOpt', $pommelOpt, PDO::PARAM_STR);
                $query->bindValue(':pommelFin', $pommelFin, PDO::PARAM_STR);
                $query->bindValue(':ringsComp', $ringsComp, PDO::PARAM_STR);
                $query->bindValue(':ringsOpt', $ringsOpt, PDO::PARAM_STR);
                $query->bindValue(':ringsFin', $ringsFin, PDO::PARAM_STR);
                $query->bindValue(':vaultComp', $vaultComp, PDO::PARAM_STR);
                $query->bindValue(':vaultOpt', $vaultOpt, PDO::PARAM_STR);
                $query->bindValue(':vaultFin', $vaultFin, PDO::PARAM_STR);
                $query->bindValue(':pbarsComp', $pbarsComp, PDO::PARAM_STR);
                $query->bindValue(':pbarsOpt', $pbarsOpt, PDO::PARAM_STR);
                $query->bindValue(':pbarsFin', $pbarsFin, PDO::PARAM_STR);
                $query->bindValue(':hbarComp', $hbarComp, PDO::PARAM_STR);
                $query->bindValue(':hbarOpt', $hbarOpt, PDO::PARAM_STR);
                $query->bindValue(':hbarFin', $hbarFin, PDO::PARAM_STR);
                $query->bindValue(':headJudge', $headJudge, PDO::PARAM_STR);
                $query->bindValue(':record_id', $record_id, PDO::PARAM_STR);
                
                $query->execute();
                unset($_POST['edit_record']);
                
            }
            else{
                $this->errors[] = DATABASE_ERROR;
            }
        }
    }
    
    public function test() {
        $test = "<p>test1</p><br>
                 <p>test2</p>";
        return $test;
    }
}

?>