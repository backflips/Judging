<?php
require_once('Profile.php');

$login = new Profile();

if(!$login->isUserLoggedIn()){
    header('Location: login.php');
}

require_once('_header.php');
?>
<div class="container-fluid nav-spacer">
    <div class="row">
<?php
require_once('sidebar.php');
?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<?php
require_once('message_helper.php');
if(isset($_GET['profile'])) {
    if(isset($_POST['edit_profile'])) {
        require_once('edit_profile.php');
    }
    else {
        require_once('show_profile.php');
    }
}
else if(isset($_GET['account'])) {
    if(isset($_POST['edit_account'])) {
        require_once('edit_account.php');
    }
    else {
        require_once('show_account.php');
    }
}
else if(isset($_GET['certifications'])) {
    require_once('show_certifications.php');
}
else if(isset($_GET['password'])) {
    if($login->hasPasswordChanged()){
        require_once('show_account.php');
    }
    else {
        require_once('change_password.php');
    }
}
else {
    require_once('show_account.php');
}

?>
        </div>
    </div>
</div>
<?php


require_once('_footer.php')?>

