<div class="panel panel-default">
    <div class="panel-heading"><strong>Change Password</strong></div>
    <div class="panel-body">
        <form method="post" action="settings.php?password" name="passwordform" class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2">Old Password</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" id="inputOldPassword" name="oldPassword" placeholder="Old Password" required=true>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">New Password</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" id="inputNewPassword" name="newPassword" placeholder="New Password" required=true>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Re-type Password</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" id="inputConfirmPassword" name="confirmPassword" placeholder="Re-type Password" required=true>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2 col-sm-offset-2">
                <button type="submit" class="btn btn-success btn-block" name="save_password">Save</button>
            </div>
            <div class="col-xs-6 col-sm-2">
                <a class="btn btn-warning btn-block" href="settings.php?account">Cancel</a>
            </div>
        </form>
    </div>
</div>
