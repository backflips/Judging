<?php
require_once('Profile.php');
require_once("Record.php");

$login = new Profile();
if(!$login->isUserLoggedIn()){
    header('Location: login.php');
}
$record = new userRecord();
if(isset($_POST['save'])) {
    require_once('make_record_pdf.php');
}
else {
require_once('_header.php');
?>
<div class="container">
    <?php require_once('message_helper.php'); ?>
    <div class="page-header">
        <h1>Judging Records</h1>
    </div>
</div>
<?php
if(isset($_GET['add_record'])) {
    require_once('add_record.php');
}
else if(isset($_POST['del_record']) && filter_var($_POST['del_record'], FILTER_VALIDATE_INT)) {
    require_once('delete_record_confirm.php');
}
else if(isset($_POST['edit_record']) && filter_var($_POST['edit_record'], FILTER_VALIDATE_INT)) {
    require_once('edit_record.php');
}
else {
    require_once('generate_record_table.php');
}
?>


<?php require_once('_footer.php');
}?>